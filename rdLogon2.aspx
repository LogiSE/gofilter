<%@ Page Language="VB" %>
<%@ import Namespace="System.Web" %>
<%@ import Namespace="System.Xml.XPath" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Login &middot; InfoGo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
	<link type="text/css" rel="stylesheet" href="http://localhost/SalesDemo/_Themes/Hydra-Helium/css/bootstrap.css" />
	<style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 550px;
        padding: 10px 30px 10px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
	  .account-container-2 {
		width:450px; 
		display: block;
		margin: 60px auto 0 auto;
		text-align:center;
	}
	#divExpressLogin{
		margin-top:30px;
	}
	#divButton{
		margin-top:-30px;
	}
  #mainTable {
    margin-top: 30px;
  }
	.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
		border-top: 0px solid #dddddd;
	}
	
	.table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
		padding: 0px;
		line-height: 1.428571429;
		vertical-align: center;
		border-top: 0px solid #dddddd;
	}
    </style>
   
  </head>

  <body onkeypress="if (event.keyCode==13){frmLogon.submit()}" onload="document.getElementById('rdUsername').focus()">
	
	<div class="account-container-2 span4">
       <h2>Logi Analytics GoStand</h2><h4>Powered by SSRM</h4>
	
	
<body onkeypress="if (event.keyCode==13){frmLogon.submit()}" >	
		<div class="form-signin">
    <table class="table" id="mainTable">
		
			<tr>
				<td id="mainCell" >
					<div id="logonPanel">
			
							<tr>
								<td id="formCell"> 
<form id="frmLogon" method="post">
									  
                <table>
                	    <!-- VPs -->
                                        <tr>
                                          <td align="left" width="200"><b>VP Global Sales</b></td>                                    
                                          <td colspan="2" width="200"><div align="right"><a href="rdPage.aspx?rdPage=GoHome&rdType=Employee&amp;rdUsername=Welcker&amp;rdPassword=Brian&amp;rdFormLogon=True">Welcker</a></div></td>
                                          <td></td>
                                        </tr>
                                       
                       <!-- Managers -->
                                        <tr>
                                          <td align="left" width="200"><b>Regional Sales Managers</b> </td>
                                          <td colspan="2" width="200"><div align="right"><a href="rdPage.aspx?rdPage=GoHome&rdType=Employee&amp;rdUsername=Jiang&amp;rdPassword=Stephen&amp;rdFormLogon=True">Jiang (North America)</a></div></td>
                                        </tr>
                                          <tr>
                                          <td></td>
                                          <td><div align="right"><a href="rdPage.aspx?rdPage=GoHome&rdType=Employee&amp;rdUsername=Alberts&amp;rdPassword=Amy&amp;rdFormLogon=True">Alberts (Europe)</a></div></td>
                                          </tr> 
                                          <tr>
                                          <td></td>
                                          <td><div align="right"><a href="rdPage.aspx?rdPage=GoHome&rdType=Employee&amp;rdUsername=Abbas&amp;rdPassword=Syed&amp;rdFormLogon=True">Abbas (Pacific)</a></div></td>
                                        </tr>
                       	<!-- Reps -->
                                        <tr>
                                          <td align="left" ><b>Sales Reps</b></td>
                                          <td><div align="right"><a href="rdPage.aspx?rdPage=GoHome&rdType=Employee&amp;rdUsername=Campbell&amp;rdPassword=David&amp;rdFormLogon=True">Campbell</a></div></td>
                                        </tr>
                                        <tr>
                                          <td></td>
                                          <td><div align="right"><a href="rdPage.aspx?rdPage=GoHome&rdType=Employee&amp;rdUsername=Pak&amp;rdPassword=Jae&amp;rdFormLogon=True">Pak </a></div></td>
                                        </tr>
                                         <tr>
                                          <td></td>
                                          <td><div align="right"><a href="rdPage.aspx?rdPage=GoHome&rdType=Employee&amp;rdUsername=Saraiva&amp;rdPassword=Jos%C3%A9&amp;rdFormLogon=True">Saraiva</a></div></td>
                                        </tr>
                                         <tr>
                                          <td></td>
                                          <td><div align="right"><a href="rdPage.aspx?rdPage=GoHome&rdType=Employee&amp;rdUsername=Vargas&amp;rdPassword=Garrett&amp;rdFormLogon=True">Vargas</a></div></td>
                                        </tr>
                                         <tr>
                                          <td></td>
                                          <td><div align="right"><a href="rdPage.aspx?rdPage=GoHome&rdType=Employee&amp;rdUsername=Reiter&amp;rdPassword=Tsvi&amp;rdFormLogon=True">Reiter</a></div></td>
                                        </tr>
										<tr>
                                          <td></td>
                                          <td><div align="right"><a href="rdPage.aspx?rdPage=GoHome&rdType=Employee&amp;rdUsername=Ito&amp;rdPassword=Shu&amp;rdFormLogon=True">Ito</a></div></td>
                                        </tr>
                                      </table>
                    <p align="right">&nbsp;</p>
				    </form>								</td>
							</tr>
							<tr>
							  <td id="formCell3"><div align="center"><a href="rdLogon.aspx" class='btn btn-large btn-primary'>Regular Login</a></div></td>
						  </tr>
						</table>						
					</div>
				</td>
			</tr>
		</table>
		</div> <!-- /container -->
	</body>
</html>