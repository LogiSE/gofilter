function loadReport(appName,rdLoadBookmark,rdReport,rdBookmarkCollection,rdBookmarkUserName,rdBookmarkID,rdSharedBookmarkID){
	var filtervars = "";
	var rememberFilters = "";
	var inputs = document.getElementsByTagName("Select");
	console.log("Found " + inputs.length + " inputs.");
	for(var i = 0; i < inputs.length; i++){
		var currentList = inputs[i];
		var cname = currentList.getAttribute("name");
		
		if(cname.indexOf('_') > -1){
			var cvalue = currentList.options[inputs[i].selectedIndex].value;
			var pair = cname.replace("Filter_","") + ':' + cvalue + '~';
			rememberFilters += "&"+cname.replace("Filter","")+"="+cvalue;
			filtervars += pair;
		}
		

	}
	
	filtervars = filtervars.substring(0,filtervars.length-1);

	var url = "/"+appName+"/rdPage.aspx?rdLoadBookmark=" + rdLoadBookmark + "&rdReport=" + rdReport + "&rdBookmarkCollection=" + rdBookmarkCollection + "&rdBookmarkUserName=" + rdBookmarkUserName + "&rdBookmarkID=" + rdBookmarkID + "&rdSharedBookmarkID="+rdSharedBookmarkID+"&FilterVariables="+filtervars+rememberFilters;
	console.log("Link URL is: " + url);
	location.href = url;

}