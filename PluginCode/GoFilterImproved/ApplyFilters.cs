﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GoFilterImproved
{
    public class ApplyFilters
    {
        public XmlDocument definition = null;
        public ApplyFilters(XmlDocument doc)
        {
            definition = doc;

        }
        public XmlDocument appendFilters(string filters)
        {
            List < string[] > fltrList = new List<string[]>();
            string[] filterArray = filters.Split('~');
            foreach(string filter in filterArray)
            {
                string[] filterPair = { filter.Split(':')[0], filter.Split(':')[1] };
                fltrList.Add(filterPair);
            }

            foreach(string[] pair in fltrList)
            {
                XmlNodeList columns = definition.GetElementsByTagName("SqlColumn");
                foreach(XmlNode col in columns)
                {
                    string key = pair[0];
                    string val = pair[1];

                    if (val.Length > 0)
                    {
                        XmlElement c = (XmlElement)col;
                        if (c.GetAttribute("ID").Equals(key))
                        {
                            string realColName = c.GetAttribute("DataColumn");
                            XmlElement condition = this.buildFilter(realColName, val);
                            XmlElement parent = (XmlElement)c.ParentNode;
                            parent.AppendChild(condition);
                        }
                    }
                    
                }
            }


            return definition;
        }

        public XmlElement buildFilter(string column, string value)
        {
            XmlElement filter = definition.CreateElement("SqlConditionFilter");

            XmlAttribute expression = definition.CreateAttribute("SqlExpression");
            expression.Value = column + " = '" + value + "'";
            filter.SetAttributeNode(expression);

            
            return filter;
        }

        public Boolean canFilter(string table, string column, XmlElement dataLayer)
        {

            XmlNodeList nl = dataLayer.GetElementsByTagName("SqlColumn");
            Boolean result = false;
            foreach(XmlNode n in nl)
            {
                XmlElement e = (XmlElement)n;
                if (e.GetAttribute("DataColumn").Equals(column))
                {
                    XmlElement eParent = (XmlElement)e.ParentNode;
                    if (eParent.GetAttribute("QueryBuilderTableID").Equals(table))
                    {
                        result = true;
                    }
                }
            }

            return result;
        }
    }
}
