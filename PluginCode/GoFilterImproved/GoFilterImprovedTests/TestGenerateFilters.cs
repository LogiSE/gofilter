﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using System.Diagnostics;
using GoFilterImproved;
using System.Text;

namespace GoFilterImprovedTests
{
    [TestClass]
    public class TestGenerateFilters
    {
        //Globals
        public string filePath = "C:\\temp\\debug.xml";
        public string outFilePath = "C:\\temp\\debugOut.xml";
        public string metadataPath = "C:\\temp\\testMetadata.xml";
        public string altMetadataPath = "C:\\temp\\simpleMetadata.lgx";
        public string altDefinitionPath = "C:\\temp\\testDefinitionUnderscore.xml";
        public string connectionid = "AdventureWorks";
        public XmlDocument readyTest(string filePath)
        {
            XmlDocument d = new XmlDocument();
            d.Load(filePath);

            return d;
        }

        //Test the constructor
        [TestMethod]
        public void testConstruct()
        {
            GenerateFilters g = new GenerateFilters(readyTest(filePath), connectionid);
            Assert.AreEqual(connectionid, g.connID);
        }

        //Test build filter options
        [TestMethod]
        public void testBuildFilterOpts()
        {
            GenerateFilters g = new GenerateFilters(readyTest(filePath), connectionid);
            XmlElement output = g.buildFilterOptions(metadataPath);
            Assert.IsTrue(output.ChildNodes.Count > 1);

            //Print test doc
            XmlNode n = g.definition.ChildNodes[0];
            n.AppendChild(output);
            g.definition.Save(outFilePath);
        }

        //Test build filter options on metadata with underscores
        [TestMethod]
        public void testBuildFilterOpts2()
        {
            GenerateFilters g = new GenerateFilters(readyTest(altDefinitionPath), connectionid);
            XmlElement output = g.buildFilterOptions(altMetadataPath);
            Assert.IsTrue(output.ChildNodes.Count > 1);

            //Print test doc
            XmlNode n = g.definition.ChildNodes[0];
            n.AppendChild(output);
            g.definition.Save("C:\\temp\\debugOutUnderscore.xml");
        }

        [TestMethod]
        public void testAssembleFilter()
        {
            XmlDocument metadata = new XmlDocument();
            metadata.Load(metadataPath);
            Debug.WriteLine("Document loaded with " + metadata.ChildNodes.Count + " elements.");
            GenerateFilters g = new GenerateFilters(readyTest(filePath), connectionid);
            XmlElement fltr = g.assembleFilter(metadataPath, "SalesRep", "LastName");
            Assert.AreEqual("LastName", fltr.GetAttribute("OptionCaptionColumn"));
            Assert.AreEqual(1, fltr.ChildNodes.Count);

            XmlElement data = (XmlElement)fltr.ChildNodes[0];
            Assert.AreEqual("select distinct [LastName] from (Select Sales.SalesPerson.SalesPersonID, HumanResources.Employee.EmployeeID, HumanResources.Employee.ContactID, HumanResources.Employee.ManagerID, HumanResources.vEmployeeDepartment.LastName, HumanResources.vEmployeeDepartment.FirstName, HumanResources.vEmployeeDepartment.Department, HumanResources.vEmployeeDepartment.JobTitle, Sales.SalesTerritory.[Group], Sales.SalesTerritory.Name From Sales.SalesPerson Inner Join Sales.SalesTerritory On Sales.SalesTerritory.TerritoryID = Sales.SalesPerson.TerritoryID Inner Join HumanResources.Employee On HumanResources.Employee.EmployeeID = Sales.SalesPerson.SalesPersonID Inner Join HumanResources.vEmployeeDepartment On HumanResources.vEmployeeDepartment.EmployeeID = HumanResources.Employee.EmployeeID Where Sales.SalesPerson.SalesPersonID IN (@Session.employeeDownline~)) as t", data.GetAttribute("Source"));
           

        }

        //Test getting query for custom tables
        [TestMethod]
        public void testGetQuery()
        {
            GenerateFilters g = new GenerateFilters(readyTest(filePath), connectionid);
            string query = g.getQuery("SalesRep", "LastName", metadataPath);
            Debug.WriteLine("Final Query: " + query);
            Assert.AreEqual("select distinct [LastName] from (Select Sales.SalesPerson.SalesPersonID, HumanResources.Employee.EmployeeID, HumanResources.Employee.ContactID, HumanResources.Employee.ManagerID, HumanResources.vEmployeeDepartment.LastName, HumanResources.vEmployeeDepartment.FirstName, HumanResources.vEmployeeDepartment.Department, HumanResources.vEmployeeDepartment.JobTitle, Sales.SalesTerritory.[Group], Sales.SalesTerritory.Name From Sales.SalesPerson Inner Join Sales.SalesTerritory On Sales.SalesTerritory.TerritoryID = Sales.SalesPerson.TerritoryID Inner Join HumanResources.Employee On HumanResources.Employee.EmployeeID = Sales.SalesPerson.SalesPersonID Inner Join HumanResources.vEmployeeDepartment On HumanResources.vEmployeeDepartment.EmployeeID = HumanResources.Employee.EmployeeID Where Sales.SalesPerson.SalesPersonID IN (@Session.employeeDownline~)) as t", query);

        }

        //Test getting query for regular tables
        [TestMethod]
        public void testGetSimpleQuery()
        {
            GenerateFilters g = new GenerateFilters(readyTest(filePath), connectionid);
            string query = g.getQuery("Sales.Customer", "CustomerID", altMetadataPath);
            Debug.WriteLine("Final query: " + query);
            Assert.AreEqual("select distinct [CustomerID] from Sales.Customer", query);

        }
    }
}
