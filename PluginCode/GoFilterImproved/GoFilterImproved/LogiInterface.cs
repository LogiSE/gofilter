﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using rdPlugin;
using System.Collections;
using System.Xml;
using GoFilterImproved;

namespace GoFilterImproved
{
    public class LogiInterface
    {
        public void triggerGenerateFilters(rdServerObjects rdObjects)
        {
            rdObjects.AddDebugMessage("Plugin started");

            Hashtable ht = rdObjects.PluginParameters;
            string AppMetadataPath = (string)ht["AppMetadataPath"];
            string ConnectionID = (string)ht["ConnectionID"];
            string Script = (string)ht["Script"];

            XmlDocument definition = new XmlDocument();
            definition.LoadXml(rdObjects.CurrentDefinition);

            GenerateFilters g = new GenerateFilters(definition, ConnectionID);

            XmlElement output = g.buildFilterOptions(AppMetadataPath);
            definition.ChildNodes[0].AppendChild(output);

            rdObjects.CurrentDefinition = definition.OuterXml;
            

        }
    }
}
