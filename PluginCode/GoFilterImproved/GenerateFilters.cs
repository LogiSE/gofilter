﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GoFilterImproved
{
    public class GenerateFilters
    {
        public XmlDocument definition;
        public string connID;

        //Constructor
        public GenerateFilters(XmlDocument d, string cid)
        {
            definition = d;
            connID = cid;

        }

        public XmlElement buildFilterOptions(string metadataPath, string script)
        {
            //Build a list of SqlColumn elements
            XmlNodeList sqlColumns = definition.GetElementsByTagName("SqlColumn");

            List<string> columnList = new List<string>();
            foreach (XmlNode ele in sqlColumns)
            {
                XmlElement xele = (XmlElement)ele;
                if (xele.GetAttribute("DataType").Equals("Text")) //Restricted to just categorical filters for now.
                {
                    string column = xele.GetAttribute("DataColumn");
                    XmlElement xeleParent = (XmlElement)xele.ParentNode;
                    string table = xeleParent.GetAttribute("QueryBuilderTableID");

                    //add datapointer to column list if not exists already
                    columnList.Add(table + "~" + column);

                }
                
                
            }

            columnList = columnList.Distinct().ToList();

            //Put the filters in a popup panel
            XmlElement popup = definition.CreateElement("PopupPanel");
            XmlAttribute popupID = definition.CreateAttribute("ID");
            popupID.Value = "FilterPopup";
            popup.SetAttributeNode(popupID);
            XmlAttribute height = definition.CreateAttribute("Height");
            height.Value = "600";
            popup.SetAttributeNode(height);
            XmlAttribute width = definition.CreateAttribute("Width");
            width.Value = "600";
            popup.SetAttributeNode(width);
            XmlAttribute caption = definition.CreateAttribute("Caption");
            caption.Value = "Manage Filters";
            popup.SetAttributeNode(caption);

            //Make a container for the filters.
            XmlElement filterDiv = definition.CreateElement("Division");
            XmlAttribute filterDivID = definition.CreateAttribute("ID");
            filterDivID.Value = "filterDiv";
            filterDiv.SetAttributeNode(filterDivID);
            XmlAttribute filterDivTag = definition.CreateAttribute("HtmlDiv");
            filterDivTag.Value = "True";
            filterDiv.SetAttributeNode(filterDivTag);

            //Add the "Filter" button to the filterdiv
            XmlElement btnGo = definition.CreateElement("Button");

            //Action for the button
            XmlElement action = definition.CreateElement("Action");
            XmlAttribute type = definition.CreateAttribute("Type");
            type.Value = "Javascript";
            action.SetAttributeNode(type);
            XmlAttribute actionID = definition.CreateAttribute("ID");
            actionID.Value = "actLoadReport";
            action.SetAttributeNode(actionID);
            XmlAttribute js = definition.CreateAttribute("Javascript");
            js.Value = script;
            action.SetAttributeNode(js);
            btnGo.AppendChild(action);


            XmlAttribute btnID = definition.CreateAttribute("ID");
            btnID.Value = "btnGo";
            btnGo.SetAttributeNode(btnID);

            XmlAttribute cap = definition.CreateAttribute("Caption");
            cap.Value = "Filter";
            btnGo.SetAttributeNode(cap);

            XmlAttribute btnClass = definition.CreateAttribute("Class");
            btnClass.Value = "btn btn-default";
            btnGo.SetAttributeNode(btnClass);

            XmlAttribute tooltip = definition.CreateAttribute("Tooltip");
            tooltip.Value = "Apply the filters.";
            btnGo.SetAttributeNode(tooltip);

            filterDiv.AppendChild(btnGo);

            //Append filters
            foreach(string item in columnList)
            {
                string[] tblcol = item.Split('~');
                //Add a new line after each filter
                XmlElement newLine = definition.CreateElement("LineBreak");
                XmlAttribute lineCount = definition.CreateAttribute("LineCount");
                lineCount.Value = "2";
                newLine.SetAttributeNode(lineCount);
                filterDiv.AppendChild(newLine);
                Debug.WriteLine("Table: " + tblcol[0] + " Column: " + tblcol[1]);

                filterDiv.AppendChild(this.assembleFilter(metadataPath, tblcol[0], tblcol[1]));


            }

            popup.AppendChild(filterDiv);


            return popup;
        }


        public string getQuery(string table, string column, string metadataPath)
        {
            string query = "";
            XmlDocument metadata = new XmlDocument();
            metadata.Load(metadataPath);

            XmlNodeList nl = metadata.GetElementsByTagName("Table");
            for (int i = 0; i < nl.Count; i++)
            {
                XmlElement tblElement = (XmlElement)nl[i];
                if (tblElement.GetAttribute("TableName").Equals(table))
                {

                    if (tblElement.HasAttribute("SqlSource"))
                    {
                        query = "select distinct [" + column + "] from (" + tblElement.GetAttribute("SqlSource") + ") as t";
                    }
                    else
                    {
                        query = "select distinct [" + column + "] from " + table;
                    }
                }
                else if (tblElement.GetAttribute("TableName").Equals(table.Replace('_', '.')))
                {
                    table = table.Replace('_', '.');
                    if (tblElement.HasAttribute("SqlSource"))
                    {
                        query = "select distinct [" + column + "] from (" + tblElement.GetAttribute("SqlSource") + ") as t";
                    }
                    else
                    {
                        query = "select distinct [" + column + "] from " + table;
                    }

                }


            }



            return query;
        }

        public XmlElement assembleFilter(string metadata, string table, string column)
        {

            /***Build the filter element***/

            XmlElement filter = definition.CreateElement("InputSelectList");

            //IncludeBlank attribute
            XmlAttribute includeBlank = definition.CreateAttribute("IncludeBlank");
            includeBlank.Value = "True";
            filter.SetAttributeNode(includeBlank);

            //OptionValueColumn
            XmlAttribute value = definition.CreateAttribute("OptionValueColumn");
            value.Value = column;
            filter.SetAttributeNode(value);
            
            //OptionCaptionColumn attribute
            XmlAttribute caption = definition.CreateAttribute("OptionCaptionColumn");
            caption.Value = column;
            filter.SetAttributeNode(caption);

            //Default value
            XmlAttribute defaultvalue = definition.CreateAttribute("DefaultValue");
            defaultvalue.Value = "@Request." + table + "_" + column + "~";
            filter.SetAttributeNode(defaultvalue);

            //ID
            XmlAttribute id = definition.CreateAttribute("ID");
            id.Value = table + "_" + column;
            filter.SetAttributeNode(id);

            //Caption
            XmlAttribute visibleCaption = definition.CreateAttribute("Caption");
            visibleCaption.Value = column;
            filter.SetAttributeNode(visibleCaption);

            /***Build the datalayer***/

            XmlElement dl = definition.CreateElement("DataLayer");

            //Type attribute
            XmlAttribute type = definition.CreateAttribute("Type");
            type.Value = "SQL";
            dl.SetAttributeNode(type);

            //Source attribute
            XmlAttribute source = definition.CreateAttribute("Source");
            source.Value = this.getQuery(table, column, metadata);
            dl.SetAttributeNode(source);

            //ConnectionID attribute
            XmlAttribute connectionID = definition.CreateAttribute("ConnectionID");
            connectionID.Value = connID;
            dl.SetAttributeNode(connectionID);

            //ID Attribute
            XmlAttribute dataId = definition.CreateAttribute("ID");
            dataId.Value = "FilterData_" + table + "_" + column;
            dl.SetAttributeNode(dataId);

            /**Return the elements**/
            filter.AppendChild(dl);

            return filter;
        }
    }
}
