﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using GoFilterImproved;

namespace GoFilterImprovedTests
{
    [TestClass]
    public class TestApplyFilters
    {
        //Globals
        public string filePath = "C:\\temp\\debug.xml";
        public string outFilePath = "C:\\temp\\debugOut.xml";

        //General filter sample
        public string filterSample = "Products_Category:Bikes~SalesRep_LastName:Pak";

        //Fixed filter sample
        public string table = "Sales";
        public string column = "SalesOrderID";
        public string value = "1";

        public XmlDocument readyTest(string filePath)
        {
            XmlDocument d = new XmlDocument();
            d.Load(filePath);

            return d;
        }

        [TestMethod]
        public void TestCanFilter()
        {
            ApplyFilters a = new ApplyFilters(readyTest(filePath));
            XmlDocument d = readyTest(filePath);
            XmlNodeList dataLayers = d.GetElementsByTagName("DataLayer");
            XmlElement e = null;
            int count = 0;
            while(e == null)
            {
                XmlElement dlc = (XmlElement) dataLayers[count];
                if (dlc.GetAttribute("Type").Equals("ActiveSQL"))
                {
                    e = dlc;
                } else
                {
                    count++;
                }
            }

            Boolean output = a.canFilter(table, column, e);
            Assert.AreEqual(true, output);

        }

        [TestMethod]
        public void testBuildFilter()
        {

            XmlDocument d = readyTest(filePath);

            ApplyFilters a = new ApplyFilters(d);
            XmlElement filter = a.buildFilter(column, value);
            Assert.IsTrue(filter.GetAttribute("SqlExpression").Length > 0);

            //Print result to check output
            
            d.ChildNodes[0].AppendChild(filter);
            d.Save(outFilePath);
        }

        [TestMethod]
        public void testAppendFilters()
        {
            XmlDocument d = readyTest(filePath);

            ApplyFilters a = new ApplyFilters(d);
            XmlDocument output = a.appendFilters(filterSample);
            Assert.IsTrue(output.GetElementsByTagName("SqlConditionFilter").Count > 0);

            //Print result to check output
            output.Save(outFilePath);

        }
    }
}
