﻿using System;
using rdPlugin;
using System.Xml;
using System.Collections;

namespace GoFilter
{
    public class GoFilter
    {
        public string AppMetadataPath;
        public string Script;

        //Method to grab the xml document
        public XmlDocument getDoc(ref rdServerObjects rdObjects)
        {
            XmlDocument xmlReport = new XmlDocument();
            xmlReport.LoadXml(rdObjects.CurrentDefinition);
            return xmlReport;
        }

        public void addFilter(ref rdServerObjects rdObjects)
        {
            //Get the parameters
            Hashtable ht = rdObjects.PluginParameters;
            String filters = (String)ht["filterVars"];
            rdObjects.AddDebugMessage("Filters: " + filters);

            if (filters.Length > 0)
            {
                //Parse filters
                String[] filterList = filters.Split('~');


                //Get the current xml document
                XmlDocument d = getDoc(ref rdObjects);



                //Get all the datalayers, then build the condition filters to append to said datalayers. Don't append the filters if there is no value.
                XmlNodeList dataLayers = d.GetElementsByTagName("DataLayer");
                rdObjects.AddDebugMessage("Found " + dataLayers.Count + " datalayers.");
                for (int i = 0; i < dataLayers.Count; i++)
                {
                    if (dataLayers[i].Attributes["Type"].Value.Equals("ActiveSQL"))
                    {

                        for (int j = 0; j < filterList.Length; j++)
                        {
                            //Only append if filter value is not empty, and if that visual has the proper column.
                            if (!filterList[j].Split(':')[1].Equals("") && canAppend(ref rdObjects, (XmlElement)dataLayers[i], filterList[j].Split(':')[0]))
                            {
                                rdObjects.AddDebugMessage("Appending filter for " + filterList[j]);
                                XmlElement e = makeFilter(d, filterList[j].Split(':')[0], filterList[j].Split(':')[1]);
                                XmlNodeList s = dataLayers[i].ChildNodes;
                                for (int c = 0; c < s.Count; c++)
                                {
                                    if (!s[i].Name.Contains("SQL"))
                                    {
                                        dataLayers[i].InsertBefore(e, s[i]);
                                    }
                                }
                            }


                        }

                    }

                }


                //Save a debug file
                d.Save("C:\\temp\\debug.xml");

                //Return modified xml
                rdObjects.CurrentDefinition = d.OuterXml;
            } else
            {
                rdObjects.AddDebugMessage("No filters applied yet.");
            }


            
        }

        //Method to build xml attributes.
        public XmlAttribute makeAttribute(XmlDocument d, String attrName, String attrVal) {
            XmlAttribute a = d.CreateAttribute(attrName);
            a.Value = attrVal;

            return a;
        }

        //Method to make a sql condition filter
        public XmlElement makeFilter(XmlDocument d, String column, String value)
        {
            //Parse column name
            column = ReplaceFirst(column, "_", "~");
            column = column.Split('~')[1];
            
            //Create a condition filter
            XmlElement conditionFilter = d.CreateElement("SqlConditionFilter");
            XmlAttribute sqlExpression = d.CreateAttribute("SqlExpression");
            sqlExpression.Value = column + " = '" + value + "'";
            conditionFilter.SetAttributeNode(sqlExpression);


            return conditionFilter;
        }

        //Method to get the column list from a datalayer
        public Boolean canAppend(ref rdServerObjects rdObjects, XmlElement dl, String filter)
        {
            XmlNodeList nl = dl.GetElementsByTagName("SqlColumn");
            rdObjects.AddDebugMessage("Got a list of " + nl.Count + " elements.");
            Boolean append = false;
            XmlElement e = null;
            for(int i = 0; i < nl.Count; i++)
            {
                e = (XmlElement)nl[i];
                rdObjects.AddDebugMessage("Scanning " + e.GetAttribute("ID") + " for " + filter + " column.");
                if (e.GetAttribute("ID").Equals(filter))
                {
                    append = true;
                }
            }

            rdObjects.AddDebugMessage("Append value " + append);

            return append;

        }

        //Helpful method to replace the first instance of a string - this is used for removing underscores from column names.
        public string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        //Method to generate filter options
        public void genFilterOptions(ref rdServerObjects rdObjects)
        {
            XmlDocument d = getDoc(ref rdObjects);
            rdObjects.AddDebugMessage("Got the doc");
            //Get parameters
            Hashtable ht = rdObjects.PluginParameters;
            AppMetadataPath = (String)ht["AppMetadataPath"];
            Script = (String)ht["Script"];

            rdObjects.AddDebugMessage("Got the params");

            //Start by getting a list of column names
            XmlNodeList nlColumns = d.GetElementsByTagName("SqlColumn");
            rdObjects.AddDebugMessage("Found " + nlColumns.Count + " SQL Columns.");
            ArrayList columns = new ArrayList();
            string colName = "";
            rdObjects.AddDebugMessage("Still working.");
            for(int j = 0; j < nlColumns.Count; j++)
            {
                rdObjects.AddDebugMessage("Scanning attribute type " + nlColumns[j].Attributes["DataType"].Value);
                if (nlColumns[j].Attributes["DataType"].Value.Equals("Text"))
                {
                    rdObjects.AddDebugMessage("Type match for " + nlColumns[j].Attributes["ID"].Value);
                    colName = nlColumns[j].Attributes["ID"].Value;
                    if (!columns.Contains(colName))
                    {
                        columns.Add(colName);
                        rdObjects.AddDebugMessage(colName + " added to list");
                    }

                }
            }

            rdObjects.AddDebugMessage("Put columns in a list");

            //Build a container for the filters
            XmlElement filterDiv = (XmlElement)d.GetElementsByTagName("Division")[0];


            XmlElement resRow = d.CreateElement("ResponsiveRow");
            resRow.SetAttributeNode(makeAttribute(d, "CollisionBehavior", "Overlap"));
            filterDiv.AppendChild(resRow);

            //Loop through filters and call constructFilter method.
            for(int i = 0; i < columns.Count; i++)
            {
                resRow.AppendChild(constructFilter(d, columns[i].ToString()));
                rdObjects.AddDebugMessage("Filter added for " + columns[i].ToString());
            }


            rdObjects.CurrentDefinition = d.OuterXml;

        }

        public XmlElement constructFilter(XmlDocument d, String column)
        {
            //Createt the elements and set attributes.
            XmlElement filter = d.CreateElement("ResponsiveColumn");
            XmlElement list = d.CreateElement("InputSelectList");
            list.SetAttributeNode(makeAttribute(d, "OptionCaptionColumn", column.Split('_')[1]));
            list.SetAttributeNode(makeAttribute(d, "OptionValueColumn", column.Split('_')[1]));
            list.SetAttributeNode(makeAttribute(d, "ID", column + "Filter"));
            list.SetAttributeNode(makeAttribute(d, "IncludeBlank", "True"));
            list.SetAttributeNode(makeAttribute(d, "Caption", column.Split('_')[1]));
            list.SetAttributeNode(makeAttribute(d, "DefaultValue", "@Request." + column + "~"));

            //Create datalayer and build sql query.
            XmlElement dl = d.CreateElement("DataLayer");
            dl.SetAttributeNode(makeAttribute(d, "Type", "SQL"));
            String query = getQuery(column);
            dl.SetAttributeNode(makeAttribute(d, "Source", query));


            //Stack up the elements
            filter.AppendChild(list);
            list.AppendChild(dl);


            return filter;
        }

        public string getQuery(String column)
        {
            String q = "";
            String cTable = column.Split('_')[0];
            String cColumn = column.Split('_')[1];

            //Get metadata file
            XmlDocument meta = new XmlDocument();
            meta.Load(AppMetadataPath);
            XmlNodeList tables = meta.GetElementsByTagName("Table");
            XmlElement e;
            for(int i = 0; i < tables.Count; i++)
            {
                e = (XmlElement)tables[i];
                if (e.GetAttribute("TableName").Equals(cTable))
                {
                    if (e.HasAttribute("SqlSource"))
                    {
                        q = "select distinct [" + cColumn + "] from (" + e.GetAttribute("SqlSource") + ") as t";
                    } else
                    {
                        q = "select distinct [" + cColumn + "] from " + e.GetAttribute("TableName");
                    }
                }
                
            }

            return q;
        }
    }
}
